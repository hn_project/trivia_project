#pragma once

#include "User.h"
#include <thread>
#include <deque>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <iostream>

using namespace std;

class User;
class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _quesntionsTime;
	int _quesntionsNo;
	string _name;
	int _id;

	//functions 
	string getUseresAsString(vector<User*> usersV, User* user);
	void sendMessage(string msg);
	void sendMessage(User* user, string msg);

public:
	Room(int maxUsers, User* admin, string name, int qtime, int qNo, int id);
	bool joinRoom(User* user); //returns true if the user joined the room
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers(); //returns the vector of users;
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();
};