#include "RecievedMessage.h"
#include <exception>
#include <iostream>
#include <string>

RecievedMessage::RecievedMessage(SOCKET sock, int code)
{
	_sock = sock;
	_messageCode = code;
	_user = NULL;
}

RecievedMessage::RecievedMessage(SOCKET sock, int code, vector<string> value)
{
	_sock = sock;
	_messageCode = code;
	_user = NULL;
	_values = value;
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

User* RecievedMessage::getUser()
{
	return _user;
}

void RecievedMessage::setUser(User* user)
{
	_user = user;
}

int RecievedMessage::getMessageCode()
{
	return _messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return _values;
}