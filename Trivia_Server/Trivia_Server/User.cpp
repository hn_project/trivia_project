#include "User.h"
#include "Helper.h"

#include <exception>
#include <iostream>
#include <string>

using namespace std;
User::User(string name, SOCKET s)
{
	_userName = name;
	_sock = s;
	//_currGame = NULL;
	_currRoom = NULL;
}

void User::send(string message)
{
	Helper::sendData(_sock, message);
}

string User::getUserName()
{
	return _userName;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

/*Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* gm)
{
	_currGame = gm;
	_currRoom = NULL;
}

void User::clearGame()
{
	_currGame = NULL;
}*/

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionsTime)
{
	//check if the user exist in the room

	if (_currRoom->getId() == roomId && _currRoom->getName() == roomName && _currRoom->getQuestionsNo() == questionsNo)
	{
		//if the user exists
		Helper::sendData(_sock, "1141");
		return false;
	}

	else //create room
	{
		_currRoom = new Room(maxUsers, this, roomName, questionsTime, questionsNo, roomId);
		Helper::sendData(_sock, "1140");
		return true;
	}

}

bool User::joinRoom(Room* newRoom)
{
	//if the user already in a room return false
	if (_currRoom)
		return false;

	return _currRoom->joinRoom(this); //?// �� ���� ����

}

void User::leaveRoom()
{
	if (_currRoom)
	{
		_currRoom->leaveRoom(this);
		_currRoom = NULL;
	}
}

int User::closeRoom()
{
	if (_currRoom == NULL) //if the user doesnt exist in the room
		return -1;

	_currRoom->closeRoom(this);
	//������ ����� �� ���� ��������
	_currRoom = NULL;
	
	return _currRoom->getId();
}

/*
bool User::leaveGame()
{
if (_currGame)
{
return (_currGame.leaveGame(this));
}
else
return false;
}
*/