#include "Room.h"
#include <exception>
#include <iostream>
#include <string>

Room::Room(int maxUsers, User* admin, string name, int qtime, int qNo, int id)
{
	_maxUsers = maxUsers;
	_admin = admin;
	_name = name;
	_quesntionsTime = qtime;
	_quesntionsNo = qNo;
	_id = id;
}

bool Room::joinRoom(User* user)
{
	if (_users.size() + 1 <= _maxUsers)
	{
		_users.push_back(user);
		return true;
	}
	return false;
}

void Room::leaveRoom(User* user)
{
	std::vector<User*>::iterator it;
	for (it = _users.begin(); it != _users.end(); ++it)
	{
		if ((*it)->getUserName() != user->getUserName())
			_users.erase(it);
	}

}

int Room::closeRoom(User* user)
{
	if (user == _admin)
	{
		_users.clear();
		return 1;
	}		
	return 0;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{

}

int Room::getQuestionsNo()
{
	return _quesntionsNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

string Room::getUseresAsString(vector<User*> usersV, User* user)
{

}

void Room::sendMessage(string msg)
{

}

void Room::sendMessage(User* user, string msg)
{

}